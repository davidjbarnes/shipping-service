## Wonderment Shipment Service

### Scenario
We collect and track a lot of shipping data for our customers. A prospective customer is really interested in our product that ships using multiple carriers. This customer is in need of an API endpoint that helps them understand how many of their packages made it to their destination on a daily basis.

Other customers have been asking for similar functionality, and we’ve decided to build this integration now to close the deal with this new customer. We anticipate that we’ll build future reports for other shipment statuses (in transit, shipment delayed), so we’d like to keep track of these different shipment events to make building future reports easier.

### Solution

Technologies: Gradle, Java 11, Spring Boot, MongoDB, AWS EC2, Docker

Service located at (https://wrqnmf9e62.execute-api.us-east-1.amazonaws.com/Prod/limited_tracking_service?carrier=usps&tracking_code=9400111202435825743548) is used a mock carrier service API which returns parcel tracking information. Additionally, a series of USP, FedEx and USPS tracking numbers were provided in order to seed the database. Use the below command to seed a "one-off" parcel. Or, call the seed endpoiont and specify a carrier enum and all supplied pacels will be seeded. 

```shell
curl -X POST "http://ec2-34-222-202-51.us-west-2.compute.amazonaws.com:8080/parcel" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"carrier\": \"ups\", \"tracking_number\": \"1Z8995V60312565703\"}"
```

Upon creation of a parcel, we call the above mock service. If the tracking number does not exist, the parcel fails validation and thus the validated boolean property remains false.

Users may query the below endpoints to retrieve parcel status information:

```shell
curl -X GET "http://ec2-34-222-202-51.us-west-2.compute.amazonaws.com:8080/parcel/ups?date=2021-01-07" -H "accept: */*"
```

or

```shell
curl -X GET "http://ec2-34-222-202-51.us-west-2.compute.amazonaws.com:8080/parcel" -H "accept: */*"
```

Public API Location: http://wonderment.davidjbarnes.com:8080/swagger-ui.html or http://ec2-34-222-202-51.us-west-2.compute.amazonaws.com:8080/swagger-ui.html

ButBucket code repo: https://bitbucket.org/davidjbarnes/shipping-service

### Running the service locally

#### Docker pull:
```shell
docker pull davidjbarnes/wonderment
```

#### Docker run:
```shell
docker run -d --name wonderment --rm -p 8080:8080 davidjbarnes/wonderment
```