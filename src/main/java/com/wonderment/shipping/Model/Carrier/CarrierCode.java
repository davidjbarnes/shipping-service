package com.wonderment.shipping.Model.Carrier;

public enum CarrierCode {
    ups,
    fedex,
    usps
}
