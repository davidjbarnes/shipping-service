package com.wonderment.shipping.Model.Parcel;

import com.wonderment.shipping.Data.Parcel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ParcelResponse {

    private Iterable<Parcel> parcels;
    private int count;
}
