package com.wonderment.shipping.Model.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wonderment.shipping.Model.Carrier.CarrierCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParcelCreate {

    @NotNull(message = "carrierCode is a required field")
    @ApiModelProperty(notes = "code for carrier; FED, USPS, UPS are valid codes", required = true)
    @JsonProperty("carrier")
    private CarrierCode carrierCode;

    @NotNull(message = "trackingNumber is a required field")
    @ApiModelProperty(notes = "trackingNumber of the parcel", required = true)
    @JsonProperty("tracking_number")
    private String trackingNumber;
}
