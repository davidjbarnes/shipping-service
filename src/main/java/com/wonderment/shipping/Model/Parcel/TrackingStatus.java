package com.wonderment.shipping.Model.Parcel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrackingStatus {

    private String status;

    @JsonFormat(pattern="yyyy-MM-dd")
    @JsonProperty("status_date")
    private Date statusDate;
}
