package com.wonderment.shipping.Data;

import com.wonderment.shipping.Model.Parcel.ParcelUpdate;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@ToString(callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "parcel")
public class Parcel extends ParcelUpdate {

    @Id
    private String id;

    private boolean validated = false;
}
