package com.wonderment.shipping.Exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
@Slf4j
public class DuplicateParcelException extends RuntimeException {

    public DuplicateParcelException(String message) {
        super(message);
        log.error(message);
    }
}
