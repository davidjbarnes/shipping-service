package com.wonderment.shipping.Controller;

import com.wonderment.shipping.Data.Parcel;
import com.wonderment.shipping.Model.Carrier.CarrierCode;
import com.wonderment.shipping.Model.Parcel.ParcelCreate;
import com.wonderment.shipping.Model.Parcel.ParcelResponse;
import com.wonderment.shipping.Service.ParcelService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@Validated
public class ParcelController {

    @Autowired
    private ParcelService parcelService;

    @GetMapping(value = {
            "/parcel",
    })
    @ApiOperation("get all parcels")
    public Iterable<Parcel> getAll() {
        log.info("get all parcels");
        return parcelService.getAll();
    }

    @GetMapping(value = {
            "/parcel/{carrier}"
    })
    @ApiOperation("get all parcels")
    public ResponseEntity<ParcelResponse> getAll(@PathVariable CarrierCode carrier, @RequestParam String date) {
        log.info("get all parcels by carrier and date");
        log.info("carrier="+carrier.toString());
        log.info("dateTime="+date);

        List<Parcel> parcels = (List<Parcel>) parcelService.getAll(carrier, new DateTime(date, DateTimeZone.UTC));

        return new ResponseEntity<>(new ParcelResponse(parcels, parcels.size()), HttpStatus.OK);
    }

    @GetMapping("/parcel/seed/{carrier}")
    @ApiOperation("seed all parcels")
    public ResponseEntity<Iterable<Parcel>> seed(@PathVariable CarrierCode carrier) {
        log.info("seed all parcels");
        return new ResponseEntity<>(parcelService.seed(carrier), HttpStatus.OK);
    }

    @PostMapping("/parcel")
    @ApiOperation("create a parcel")
    public Parcel create(@Valid @RequestBody ParcelCreate parcelCreate) throws Exception {
        log.info("create a parcel");
        return parcelService.create(parcelCreate);
    }
}