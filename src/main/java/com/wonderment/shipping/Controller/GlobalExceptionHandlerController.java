package com.wonderment.shipping.Controller;

import com.wonderment.shipping.Common.ApiErrorHandler;
import com.wonderment.shipping.Exception.DuplicateParcelException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandlerController {

    @ExceptionHandler({
            DuplicateParcelException.class
    })
    protected ResponseEntity<ApiErrorHandler> handleDuplicate(DuplicateParcelException ex) {
        return new ResponseEntity<>(new ApiErrorHandler(HttpStatus.CONFLICT, ex.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler({
            Exception.class
    })
    protected ResponseEntity<ApiErrorHandler> handleAllElse(Exception ex) {
        return new ResponseEntity<>(new ApiErrorHandler(HttpStatus.I_AM_A_TEAPOT, ex.getMessage()), HttpStatus.I_AM_A_TEAPOT);
    }
}
