package com.wonderment.shipping.Repository;

import com.wonderment.shipping.Data.Parcel;
import org.joda.time.DateTime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface ParcelRepository extends MongoRepository<Parcel, String> {

    @Query("{ 'trackingNumber': ?0 }")
    Optional<Parcel> findByTrackingNumber(String trackingNumber);

    @Query("{ 'carrierCode': ?0, 'trackingStatus.statusDate': ?1 }")
    Iterable<Parcel> findAllByCarrierAndDateTime(String carrier, DateTime date);
}