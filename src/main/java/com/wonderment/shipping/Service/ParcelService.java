package com.wonderment.shipping.Service;

import com.wonderment.shipping.Data.Parcel;
import com.wonderment.shipping.Exception.DuplicateParcelException;
import com.wonderment.shipping.Model.Carrier.CarrierCode;
import com.wonderment.shipping.Model.Parcel.ParcelCreate;
import com.wonderment.shipping.Model.Parcel.ParcelUpdate;
import com.wonderment.shipping.Model.Parcel.TrackingStatus;
import com.wonderment.shipping.Repository.ParcelRepository;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Validated
@Slf4j
public class ParcelService {

    @Autowired
    private ParcelRepository parcelRepository;

    @Autowired
    private ModelMapper modelMapper;

    final RestTemplate restTemplate = new RestTemplate();

    private final String URL = "https://wrqnmf9e62.execute-api.us-east-1.amazonaws.com/Prod/limited_tracking_service?carrier={0}&tracking_code={1}";

    private SimpleDateFormat formatter =  new SimpleDateFormat("yyyy-MM-dd");

    private final String[] uspsParcels = {"9405511202555478899782",
            "9400111202555478330916",
            "9400111202555478330756",
            "9400111202555478330015",
            "9400111202555478338264",
            "9374869903506066338485",
            "9400111202555478394727",
            "9400111202555419005378",
            "9374869903506055188084",
            "9405511699000140201797",
            "9405511699000148484444",
            "9400111899223817576195",
            "9400111899223817576140",
            "9374869903506078782313",
            "9400111202435825743548",
            "9400111202555478393003",
            "9400111202555439275652",
            "9400111202435825743432",
            "9400111202555478393430",
            "9400111202555478338653",
            "9374869903506058790376",
            "9374869903506058850865",
            "9405511202435825744463",
            "9361269903506062547557",
            "9374869903506056285492",
            "9400111202435825745566",
            "9374869903506059135299",
            "9400111202555478836333",
            "9405511699000199993957",
            "9400111202555478860352",
            "9374869903506079649073",
            "9374869903506063920515",
            "9361269903506055837474",
            "92001901755477000454468833",
            "LZ560920614US",
            "9374869903506055116209",
            "92001901755477000454462602",
            "9374869903506058871693",
            "9449011202435825734012",
            "9374869903506059156034",
            "9374869903506058856768",
            "9374869903506063265470",
            "9374869903506068015995",
            "9361269903506060605105",
            "9405511699000140739641",
            "9400111202435825734751",
            "9374869903506058888967",
            "9374869903506054116361",
            "9400111699000148825206",
            "9400111202563825773774"
    };
    private final String[] fedParcels = {"781911664789",
            "781912104385",
            "781903802700",
            "781911030865",
            "781937849589",
            "781899675120",
            "781940537664",
            "781942009983",
            "781937860388",
            "781937884341",
            "781911957888",
            "781937951743",
            "781938027409",
            "781913478370",
            "781911460674",
            "781910989350",
            "781912112122",
            "781984233957",
            "781912058029",
            "781934275503",
            "781884499416",
            "781985006336",
            "781924089639",
            "61290986580620629730",
            "781912292291",
            "781912000755",
            "781909164822",
            "781912070177",
            "781984850533",
            "781911659047",
            "781924112078",
            "781924093539",
            "781912082126",
            "781911977158",
            "781911870323",
            "781911637237",
            "781911794843",
            "781912004691",
            "781902394561",
            "781913900550",
            "781898334640",
            "781903177077",
            "782040078353",
            "781897186197",
            "781897318122",
            "781896746815",
            "781914410315",
            "781914579332",
            "781903280580",
            "781903243185"
    };
    private final String[] upsParcels = {
            "1Z8995V60312565703",
            "1Z7939FF0325784213",
            "1Z7939FF0336857169",
            "1Z7939FF0331095974",
            "1Z7939FF0318912883",
            "1ZEW11070297619158",
            "1Z8995V60330436201",
            "1Z8995V60316403291",
            "1Z8995V60307878877",
            "1Z8995V60334886334",
            "1ZEW11070295047618",
            "1Z8995V60327643734",
            "1Z7939FF3614420627",
            "1Z7939FF0332193062",
            "1ZEW11070292129204",
            "1ZEW11070298398752",
            "1Z8995V60302073610",
            "1Z8995V60335520217",
            "1Z8995V60304945406",
            "1ZEW11070294070657",
            "1Z7939FF0301545936",
            "1Z7939FF0332127633",
            "1Z8995V60330801575",
            "1Z8995V60336173083",
            "1Z8995V60329597924",
            "1ZEW11070296162947",
            "1ZEW11070292979359",
            "1Z8995V60335044296",
            "1Z8995V60322387628",
            "1Z8995V60306111562",
            "1ZX92R160301491530",
            "1ZX92R160339061371",
            "1ZEW11070297283136",
            "1ZEW11070294665621",
            "1ZX92R160323602986",
            "1ZX92R160320946745",
            "1ZX92R160334172359",
            "1ZX92R160338931165",
            "1ZEW11070298439216",
            "1ZX92R160325763184",
            "1Z8995V60316641677",
            "1Z7939FF0334498846",
            "1Z7939FF0320126951",
            "1ZX92R160323797099",
            "1ZX92R160327196990",
            "1ZEW11070297769255",
            "1ZX92R160333660118",
            "1ZX92R160333089924",
            "1ZE8E8170316211954",
            "1ZX92R160300161184"
    };

    public Iterable<Parcel> getAll() {
        return this.parcelRepository.findAll();
    }

    public Iterable<Parcel> getAll(CarrierCode carrier, DateTime dateTime) {
        log.info("dateTime="+dateTime.toString());
        return this.parcelRepository.findAllByCarrierAndDateTime(carrier.toString(), dateTime);
    }

    public Parcel create(@Valid ParcelCreate parcelCreate) throws Exception {
        log.info("create: packageCreate="+ parcelCreate.toString());

        Optional<Parcel> optionalParcel = parcelRepository.findByTrackingNumber(parcelCreate.getTrackingNumber());
        if(optionalParcel.isPresent()) {
            throw new DuplicateParcelException("duplicate parcel found");
        } else {
            Parcel parcel = this.save(modelMapper.map(parcelCreate, Parcel.class));
            ParcelUpdate parcelUpdate = this.fetch(parcel);
            TrackingStatus trackingStatus = new TrackingStatus();
            trackingStatus.setStatus(parcelUpdate.getTrackingStatus().getStatus());
            trackingStatus.setStatusDate(parcelUpdate.getTrackingStatus().getStatusDate());
            parcel.setValidated(true);
            parcel.setTrackingStatus(trackingStatus);
            return this.save(parcel);
        }
    }

    private Parcel save(Parcel _parcel) {
        Parcel parcel = this.parcelRepository.save(_parcel);
        log.info("save: parcel="+parcel.toString());
        return parcel;
    }

    private Parcel update(ParcelUpdate parcelUpdate) {
        Parcel parcel = this.parcelRepository.save(modelMapper.map(parcelUpdate, Parcel.class));
        log.info("update: parcel="+parcel.toString());
        return parcel;
    }

    public Iterable<Parcel> seed(CarrierCode carrier) {
        List<Parcel> parcels = new ArrayList<>();

        String[] parcelsForParsing = carrier.equals(CarrierCode.ups) ? upsParcels : carrier.equals(CarrierCode.fedex) ? fedParcels : uspsParcels;

        Arrays.stream(upsParcels).forEach(trackingNumber -> {
            try {
                parcels.add(this.create(new ParcelCreate(CarrierCode.ups, trackingNumber)));
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        });

        return parcels;
    }

    private ParcelUpdate fetch(Parcel parcel) throws Exception {
        String url = MessageFormat.format(this.URL, parcel.getCarrierCode(), parcel.getTrackingNumber());
        log.info("url=" + url);
        try {
            ParcelUpdate parcelUpdate = this.restTemplate.getForObject(url, ParcelUpdate.class);
            log.info(parcelUpdate.toString());
            return parcelUpdate;
        } catch (Exception ex) {
            throw new Exception("invalid parcel");
        }
    }
}
